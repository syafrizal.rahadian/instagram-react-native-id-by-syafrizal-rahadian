import {SafeAreaView, Text, View, Image, ScrollView} from 'react-native';
import React from 'react';
import styles from './HomeStyle';
import ViewMoreText from 'react-native-view-more-text';

export default function HomeScreen() {
  const B = props => <Text style={{fontWeight: 'bold'}}>{props.children}</Text>;

  const renderViewMore = onPress => {
    return (
      <Text onPress={onPress} style={{color: 'grey', marginHorizontal: 10}}>
        More
      </Text>
    );
  };

  const renderViewLess = onPress => {
    return (
      <Text onPress={onPress} style={{color: 'grey', marginHorizontal: 10}}>
        Less
      </Text>
    );
  };

  return (
    <SafeAreaView>
      <View style={styles.container}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Image
            source={require('../../../images/camera.png')}
            style={{width: 25, height: 25}}
          />
          <Image
            source={require('../../../images/Instagram_logo.png')}
            style={{width: 100, height: 30}}
          />
          <Image
            source={require('../../../images/messenger.png')}
            style={{width: 25, height: 25}}
          />
        </View>
      </View>

      <ScrollView>
        <View style={styles.container}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={{flexDirection: 'row'}}>
              <View>
                <View>
                  <Image
                    source={{uri: 'https://picsum.photos/100/100?random=1'}}
                    style={{width: 60, height: 60, borderRadius: 30, margin: 5}}
                  />
                  <Image
                    source={require('../../../images/plus.png')}
                    style={{
                      width: 20,
                      height: 20,
                      position: 'absolute',
                      right: 0,
                      bottom: 0,
                    }}
                  />
                </View>
                <Text style={{alignSelf: 'center', fontSize: 12}}>
                  Your Story
                </Text>
              </View>
              <View>
                <Image
                  source={{uri: 'https://picsum.photos/100/100?random=2'}}
                  style={{
                    width: 60,
                    height: 60,
                    borderRadius: 30,
                    margin: 5,
                    borderWidth: 2,
                    borderColor: '#e95950',
                  }}
                />
                <Text style={{alignSelf: 'center', fontSize: 12}}>Adi</Text>
              </View>
              <View>
                <Image
                  source={{uri: 'https://picsum.photos/100/100?random=3'}}
                  style={{
                    width: 60,
                    height: 60,
                    borderRadius: 30,
                    margin: 5,
                    borderWidth: 2,
                    borderColor: '#e95950',
                  }}
                />
                <Text style={{alignSelf: 'center', fontSize: 12}}>Bella</Text>
              </View>
              <View>
                <Image
                  source={{uri: 'https://picsum.photos/100/100?random=4'}}
                  style={{
                    width: 60,
                    height: 60,
                    borderRadius: 30,
                    margin: 5,
                    borderWidth: 2,
                    borderColor: '#e95950',
                  }}
                />
                <Text style={{alignSelf: 'center', fontSize: 12}}>Charlie</Text>
              </View>
              <View>
                <Image
                  source={{uri: 'https://picsum.photos/100/100?random=5'}}
                  style={{
                    width: 60,
                    height: 60,
                    borderRadius: 30,
                    margin: 5,
                    borderWidth: 2,
                    borderColor: '#e95950',
                  }}
                />
                <Text style={{alignSelf: 'center', fontSize: 12}}>Dedi</Text>
              </View>
              <View>
                <Image
                  source={{uri: 'https://picsum.photos/100/100?random=6'}}
                  style={{
                    width: 60,
                    height: 60,
                    borderRadius: 30,
                    margin: 5,
                    borderWidth: 2,
                    borderColor: '#e95950',
                  }}
                />
                <Text style={{alignSelf: 'center', fontSize: 12}}>Ello</Text>
              </View>
            </View>
          </ScrollView>
        </View>

        <View style={styles.container2}>
          <View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Image
                  source={{uri: 'https://picsum.photos/100/100?random=7'}}
                  style={{
                    width: 40,
                    height: 40,
                    borderRadius: 20,
                    margin: 5,
                  }}
                />
                <Text style={{marginHorizontal: 5}}>Rizal</Text>
              </View>
              <Image
                source={require('../../../images/more.png')}
                style={{
                  width: 15,
                  height: 15,
                  marginHorizontal: 5,
                }}
              />
            </View>
          </View>

          <View>
            <Image
              source={{uri: 'https://picsum.photos/400/400?random=14'}}
              style={styles.mainpict}
            />
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View style={{flexDirection: 'row'}}>
              <Image
                source={require('../../../images/heart.png')}
                style={{
                  width: 20,
                  height: 20,
                  margin: 10,
                }}
              />
              <Image
                source={require('../../../images/chat.png')}
                style={{
                  width: 20,
                  height: 20,
                  margin: 10,
                }}
              />
              <Image
                source={require('../../../images/send.png')}
                style={{
                  width: 20,
                  height: 20,
                  margin: 10,
                }}
              />
            </View>
            <View>
              <Image
                source={require('../../../images/bookmark.png')}
                style={{
                  width: 20,
                  height: 20,
                  margin: 10,
                }}
              />
            </View>
          </View>
          <View style={{flexDirection: 'row', paddingHorizontal: 10}}>
            <Image
              source={{uri: 'https://picsum.photos/100/100?random=11'}}
              style={{
                width: 20,
                height: 20,
                borderRadius: 10,
              }}
            />
            <Text style={{marginHorizontal: 2}}> Liked by</Text>
            <Text style={{fontWeight: 'bold'}}> Syafrizal</Text>
          </View>
          <View>
            <ViewMoreText
              numberOfLines={2}
              renderViewMore={renderViewMore}
              renderViewLess={renderViewLess}
              textStyle={{margin: 10}}>
              <Text style={{margin: 10}}>
                <B>Rizal</B> Lorem Ipsum is simply dummy text of the printing
                and typesetting industry. Lorem Ipsum has been the industry's
                standard dummy text ever since the 1500s, when an unknown
                printer took a galley of type and scrambled it to make a type
                specimen book. It has survived not only five centuries, but also
                the leap into electronic typesetting, remaining essentially
                unchanged. It was popularised in the 1960s with the release of
                Letraset sheets containing Lorem Ipsum passages, and more
                recently with desktop publishing software like Aldus PageMaker
                including versions of Lorem Ipsum.
              </Text>
            </ViewMoreText>
          </View>
          <View>
            <Text style={{margin: 10, color: 'grey', fontSize: 9}}>
              6 HOURS AGO
            </Text>
          </View>
        </View>
        <View style={styles.container2}>
          <View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Image
                  source={{uri: 'https://picsum.photos/100/100?random=7'}}
                  style={{
                    width: 40,
                    height: 40,
                    borderRadius: 20,
                    margin: 5,
                  }}
                />
                <Text style={{marginHorizontal: 5}}>Rizal</Text>
              </View>
              <Image
                source={require('../../../images/more.png')}
                style={{
                  width: 15,
                  height: 15,
                  marginHorizontal: 5,
                }}
              />
            </View>
          </View>

          <View>
            <Image
              source={{uri: 'https://picsum.photos/400/400?random=10'}}
              style={styles.mainpict}
            />
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View style={{flexDirection: 'row'}}>
              <Image
                source={require('../../../images/heart.png')}
                style={{
                  width: 20,
                  height: 20,
                  margin: 10,
                }}
              />
              <Image
                source={require('../../../images/chat.png')}
                style={{
                  width: 20,
                  height: 20,
                  margin: 10,
                }}
              />
              <Image
                source={require('../../../images/send.png')}
                style={{
                  width: 20,
                  height: 20,
                  margin: 10,
                }}
              />
            </View>
            <View>
              <Image
                source={require('../../../images/bookmark.png')}
                style={{
                  width: 20,
                  height: 20,
                  margin: 10,
                }}
              />
            </View>
          </View>
          <View style={{flexDirection: 'row', paddingHorizontal: 10}}>
            <Image
              source={{uri: 'https://picsum.photos/100/100?random=11'}}
              style={{
                width: 20,
                height: 20,
                borderRadius: 10,
              }}
            />
            <Text style={{marginHorizontal: 2}}> Liked by</Text>
            <Text style={{fontWeight: 'bold'}}> Syafrizal</Text>
          </View>
          <View>
            <ViewMoreText
              numberOfLines={2}
              renderViewMore={renderViewMore}
              renderViewLess={renderViewLess}
              textStyle={{margin: 10}}>
              <Text style={{margin: 10}}>
                <B>Rizal</B> Lorem Ipsum is simply dummy text of the printing
                and typesetting industry. Lorem Ipsum has been the industry's
                standard dummy text ever since the 1500s, when an unknown
                printer took a galley of type and scrambled it to make a type
                specimen book. It has survived not only five centuries, but also
                the leap into electronic typesetting, remaining essentially
                unchanged. It was popularised in the 1960s with the release of
                Letraset sheets containing Lorem Ipsum passages, and more
                recently with desktop publishing software like Aldus PageMaker
                including versions of Lorem Ipsum.
              </Text>
            </ViewMoreText>
          </View>
          <View>
            <Text style={{margin: 10, color: 'grey', fontSize: 9}}>
              6 HOURS AGO
            </Text>
          </View>
        </View>
        <View style={styles.container2}>
          <View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Image
                  source={{uri: 'https://picsum.photos/100/100?random=7'}}
                  style={{
                    width: 40,
                    height: 40,
                    borderRadius: 20,
                    margin: 5,
                  }}
                />
                <Text style={{marginHorizontal: 5}}>Rizal</Text>
              </View>
              <Image
                source={require('../../../images/more.png')}
                style={{
                  width: 15,
                  height: 15,
                  marginHorizontal: 5,
                }}
              />
            </View>
          </View>

          <View>
            <Image
              source={{uri: 'https://picsum.photos/400/400?random=10'}}
              style={styles.mainpict}
            />
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View style={{flexDirection: 'row'}}>
              <Image
                source={require('../../../images/heart.png')}
                style={{
                  width: 20,
                  height: 20,
                  margin: 10,
                }}
              />
              <Image
                source={require('../../../images/chat.png')}
                style={{
                  width: 20,
                  height: 20,
                  margin: 10,
                }}
              />
              <Image
                source={require('../../../images/send.png')}
                style={{
                  width: 20,
                  height: 20,
                  margin: 10,
                }}
              />
            </View>
            <View>
              <Image
                source={require('../../../images/bookmark.png')}
                style={{
                  width: 20,
                  height: 20,
                  margin: 10,
                }}
              />
            </View>
          </View>
          <View style={{flexDirection: 'row', paddingHorizontal: 10}}>
            <Image
              source={{uri: 'https://picsum.photos/100/100?random=11'}}
              style={{
                width: 20,
                height: 20,
                borderRadius: 10,
              }}
            />
            <Text style={{marginHorizontal: 2}}> Liked by</Text>
            <Text style={{fontWeight: 'bold'}}> Syafrizal</Text>
          </View>
          <View>
            <ViewMoreText
              numberOfLines={2}
              renderViewMore={renderViewMore}
              renderViewLess={renderViewLess}
              textStyle={{margin: 10}}>
              <Text style={{margin: 10}}>
                <B>Rizal</B> Lorem Ipsum is simply dummy text of the printing
                and typesetting industry. Lorem Ipsum has been the industry's
                standard dummy text ever since the 1500s, when an unknown
                printer took a galley of type and scrambled it to make a type
                specimen book. It has survived not only five centuries, but also
                the leap into electronic typesetting, remaining essentially
                unchanged. It was popularised in the 1960s with the release of
                Letraset sheets containing Lorem Ipsum passages, and more
                recently with desktop publishing software like Aldus PageMaker
                including versions of Lorem Ipsum.
              </Text>
            </ViewMoreText>
          </View>
          <View>
            <Text style={{margin: 10, color: 'grey', fontSize: 9}}>
              6 HOURS AGO
            </Text>
          </View>
        </View>
        <View style={{height: 50}}></View>
      </ScrollView>
    </SafeAreaView>
  );
}
